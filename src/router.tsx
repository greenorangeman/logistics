import { RouterAPI } from 'dva'
import { Router, Route, Switch } from 'dva/router'
import LayoutView from './layout/LayoutView'
import Login from './pages/Login/Login'

const router = (api?: RouterAPI) => {
  if (api) {
    return (
      <Router history={api.history}>
        <Switch>
          <Route path="/login">
            <Login></Login> 
          </Route>
          <Route path="/">
            <LayoutView></LayoutView>
          </Route>
        </Switch>
      </Router>
    )
  }
  return {}
}
export default router