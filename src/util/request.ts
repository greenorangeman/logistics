import { message } from 'antd'
import axios, { AxiosResponse } from 'axios'
import { IBaseResponse } from '../type'


/* 
 * AxiosResponse接收两个泛型
 * 第一个T是规定业务的请求体类型，也就是接口定义的类型
 * 
*/
axios.interceptors.response.use((response:AxiosResponse<IBaseResponse<any>>) => {
  /* 
    * 根据我们的业务
    * 返回的code非1000时都是报错，
    * 需要前端提示出来
  */
  if (response.data.code !== 1000) {
    //提示报错信息
    message.error(response.data.msg)
    //跑出错误，阻塞后续程序进行
    throw new Error(response.data.msg)
  }
  /* 
    * 正确的话，正常返回
    * 返回给业务层的数据只希望关心业务
   */
  return response.data.data;
})



export default axios