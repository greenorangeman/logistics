import React, { useState, useLayoutEffect } from 'react';
import type { MenuProps } from 'antd';
import "./LayoutView.css"
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons';
import { Button, Layout, Menu, theme } from 'antd';
import { LayoutNav, rootSubmenuKeys } from './LayoutView.config'
import Avatar from '../assets/头像.jpg'
import { Route } from 'dva/router'
import WordDesk from '../pages/WordDesk/WordDesk'
import InquirySheet from '../pages/OrderManage/InquirySheet'
import OrderManagement from '../pages/OrderManage/OrderManagement'
import { MenuInfo } from 'rc-menu/lib/interface'
import { useHistory } from 'dva'

const { Header, Sider, Content } = Layout;

const LayoutView = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const [openKeys, setOpenKeys] = useState(['/wordDesk']);

  const onOpenChange: MenuProps['onOpenChange'] = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey!) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };
  const history = useHistory()
  const linkPage = ({ key }: MenuInfo) => {
    history.push(key)
  }
  const toLogin = () => {
    history.push('/login')
  }
  return (
    <Layout id="LayoutView">
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo">
          {
            collapsed ? (
                <div className='userAvatarMini'>
                  <img src={Avatar} alt='图片失效' />
                </div>

            ) : (
              <>
                <div className='userAvatar'>
                  <img src={Avatar} alt='图片失效' />
                </div>
                <div className='userInfo'>
                  <p>欢迎你,</p>
                  <p>userPlayer1</p>
                </div>
              </>
            )
          }
        </div>
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={['/wordDesk/personalCenter']}
          openKeys={openKeys}
          items={LayoutNav}
          onClick={linkPage}
          onOpenChange={onOpenChange}
        />
      </Sider>
      <Layout className="site-layout">
        <div className='LayoutHeader'>
          <Header style={{ padding: 0, background: colorBgContainer }}>
            {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: () => setCollapsed(!collapsed),
            })}
          </Header>
          <div className='CompanyName'>万事达 - TMS客户端</div>
          <Button type='link' onClick={toLogin} className='LayoutHeader_Btn'>退出登录</Button>
        </div>

        <Content
          style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          <Route path="/wordDesk/personalCenter"><WordDesk></WordDesk></Route>
          <Route path="/order/inquirySheet"><InquirySheet></InquirySheet></Route>
          <Route path="/order/orderManagement"><OrderManagement></OrderManagement></Route>
        </Content>
      </Layout>
    </Layout>
  )
}
export default LayoutView