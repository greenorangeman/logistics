/** 
 * 用于存放layout页面的相关配置
*/
export const LayoutNav = [
  {
    key: '/wordDesk',
    label: '工作台',
    children: [
      {
        key: '/wordDesk/personalCenter',
        label: '个人中心',
      }
    ]
  },
  {
    key: '/order',
    label: '订单管理',
    children: [
      {
        key: '/order/inquirySheet',
        label: '询价单管理',
      },
      {
        key: '/order/orderManagement',
        label: '订单管理',
      },
    ]
  },
  {
    key: '/transport',
    label: '运输管理',
    children: [
      {
        key: '/transport/chase',
        label: '在途跟踪',
      }
    ]
  },
  {
    key: '/contract',
    label: '合同管理',
    children: [
      {
        key: '/contract/contractManage',
        label: '合同管理',
      }
    ]
  },
  {
    key: '/receipt',
    label: '回单管理',
    children: [
      {
        key: '/receipt/receiptManage',
        label: '回单管理',
      }
    ]
  },
  {
    key: '/settlement',
    label: '结算管理',
    children: [
      {
        key: '/settlement/dealPayment',
        label: '订单结算',
      }
    ]
  },
  {
    key: '/invoice',
    label: '发票管理',
    children: [
      {
        key: '/invoice/invoiceManage',
        label: '发票管理',
      }
    ]
  },
  {
    key: '/systemSet',
    label: '系统设置',
    children: [
      {
        key: '/systemSet/systemSetting',
        label: '系统设置',
      }
    ]
  },
]

export const rootSubmenuKeys = ['/wordDesk','/order','/transport','/contract','/receipt','/settlement','/invoice','/systemSet']