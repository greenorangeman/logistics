/* 
  通用返回体
 */
export interface IBaseResponse<T> {
/* 
  错误码
  除1000外都是错误
 */
  code: number,
/* 
  返回的结果
 */
  msg: string,
/* 
  接口具体返回的数据
*/
  data: T
}


export enum Role {
  //用户管理角色
  USERMANAGE = 'userManage',
  //活动管理角色
  ACTIVITYMANAGE = 'activityManage'

}