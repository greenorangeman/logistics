import { Button, Form, Input, Tabs, TabsProps } from 'antd'
import { LoginParams } from './Login.type'
import './Login.scss'
import { loginApi } from './../../api/index';

const Login = () => {
    const userLogin = async (values: LoginParams) => {
    console.log(values);
    const data = await loginApi(values)
  }
  const items: TabsProps['items'] = [
    {
      key: 'login',
      label: `登录`,
      children: (
        <Form
          name="basic"
          layout='horizontal'
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 19 }}
          style={{ maxWidth: 580, height:200,display:'flex',flexDirection:'column',justifyContent:'center'}}
          // initialValues={{ remember: true }}
          onFinish={userLogin}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="用户名"
            name="username"
            rules={[{ required: true, message: '请输入用户名！' }]}
          >
            <Input />
          </Form.Item>
    
          <Form.Item
            label="密码"
            name="password"
            rules={[{ required: true, message: '请输入密码!' }]}
          >
            <Input.Password />
          </Form.Item>
            
          <div className='LoginBtn'>
            <Button type="primary" htmlType="submit">
              登录
            </Button>
          </div>
          
      </Form>),
    },
    {
      key: 'register',
      label: `注册`,
      children: `Content of Tab Pane 2`,
    }
  ];
  return (
    <div id="login">
      <div className='loginTabs'>
        <Tabs defaultActiveKey="login" items={items} centered tabBarGutter={ 200 } animated />
      </div>
    </div>
  )
}
export default Login