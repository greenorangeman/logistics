import { Role } from "../../type"

/*
  **登录传递的参数
*/
export interface LoginParams {
  username: string,
  password: string
}
/*
  *登录接口返回的参数
*/
export interface LoginResponse {
  token: string,
  roles: Role[]
}